import os
import sys
import socket
import threading
import math
import random

from message import *

PORT = int(sys.argv[1])

# Cria o socket IPv4 do servidor
s4_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s4_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s4_socket.bind(("127.0.0.1", PORT))

# Cria o socket IPv6 do servidor
s6_socket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
s6_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s6_socket.bind(("::1", PORT))

def access_thread(connection_socket, ipv):
  # Recebendo o Hello
  hello_data = connection_socket.recv(HelloMessage.get_message_size())
  hello_type, hello_channel = MessageHelper.get_message_header(hello_data)

  # Se não recebeu hello, cancele
  if hello_type != MSG_TYPE_HELLO or hello_channel != MSG_CHANNEL_CONTROL:
    print("Erro não recebeu Hello")
    return None

  file_socket = socket.socket(socket.AF_INET if ipv == 4 else socket.AF_INET6, socket.SOCK_DGRAM)
  file_socket.bind(("127.0.0.1" if ipv == 4 else "::1", 0))

  # Mandando a porta da conexão
  file_socket_port = file_socket.getsockname()[1]
  connection_socket.send(ConnectionMessage(file_socket_port).get_message_bytes())

  # Recebendo as informações do arquivo
  info_data = connection_socket.recv(InfoFileMessage.get_message_size())
  info_type, info_channel = MessageHelper.get_message_header(info_data)

  # Se não recebeu as informações do arquivo, cancele
  if info_type != MSG_TYPE_INFO_FILE or info_channel != MSG_CHANNEL_CONTROL:
    print("Erro não recebeu Info")
    return None

  # Decodificando as informações do arquivo
  file_name, file_size = InfoFileMessage.get_message_data(info_data)

  is_ascii = True
  for c in file_name:
    if ord(c) > 127:
      is_ascii = True
      break

  if file_name.count(".") != 1 or len(file_name.split(".")[1]) < 3 or not is_ascii:
    print("Formato do arquivo incorreto")
    return

  # Enviando o ok
  connection_socket.send(OkMessage().get_message_bytes())

  # Recebendo o arquivo enviado
  all_packets = recieve_file(connection_socket, file_socket, file_size)

  # Deleta o arquivo com o mesmo nome do que vamos receber
  new_file_name = "output/" + file_name
  try:
    os.remove(new_file_name)
  except OSError:
    pass

  # Cria um arquivo com os bytes recebidos
  os.makedirs(os.path.dirname(new_file_name), exist_ok=True)
  with open(new_file_name, "ab") as out_file:
    for packet in all_packets:
      if packet is not None:
        out_file.write(packet)

  # Fecha a conexão
  connection_socket.send(EndMessage().get_message_bytes())
  connection_socket.close()

# Recebendo o arquivo
def recieve_file(connection_socket, file_socket, file_size, window_size=4):
  window_start = 0

  # Vetor para armazenar os pacotes na ordem certa
  all_packets = [None for _ in range(math.ceil(file_size / 1000))]

  while True:
    # Recebe os <= 1008 bytes de uma mensagem
    file_data, _ = file_socket.recvfrom(1008)
    file_msg_type, file_msg_channel = MessageHelper.get_message_header(file_data)

    # Se for de fato uma mensagem de arquivo
    if file_msg_type == MSG_TYPE_FILE and file_msg_channel == MSG_CHANNEL_DATA:
      # Pegue as informações do cabeçalho da mensagem
      encoded_sequence_number = file_data[2:6]
      encoded_payload_size = file_data[6:8]
      encoded_payload = file_data[8:]

      # Decodificque as informações
      sequence_number = int.from_bytes(encoded_sequence_number, byteorder="big")
      payload_size = int.from_bytes(encoded_payload_size, byteorder="big")

      # Se a gente receber um pacote que está no intervalo que esperamos da janela
      # deslizante
      if 0 <= (sequence_number - window_start) < window_size:
        # Coloca no vetor de pacotes na posição correta
        all_packets[sequence_number] = encoded_payload

        # Envia a confirmação de recebimento do pacote atual
        connection_socket.send(ACKMessage(window_start).get_message_bytes())

        # Encontra qual o próximo valor para o inicio da janela deslizante
        try:
          window_start = next(i for i in range(window_start + 1, len(all_packets)) if all_packets[i] == None)
        except StopIteration:
          break
    else:
      print("Erro nao chegou tipo de arquivo")
  
  return all_packets

def listen_ipv4():
  s4_socket.listen()

  while True:
    conn, _ = s4_socket.accept()
    threading.Thread(target=access_thread, args=(conn, 4)).start()

def listen_ipv6():
  s6_socket.listen()

  while True:
    conn, _ = s6_socket.accept()
    threading.Thread(target=access_thread, args=(conn, 6)).start()

# Ouve novas conexões e cria uma thread para cada usuário
def main():
  threading.Thread(target=listen_ipv4).start()
  threading.Thread(target=listen_ipv6).start()

main()