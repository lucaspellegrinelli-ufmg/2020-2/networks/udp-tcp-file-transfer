import os
import sys
import time
import threading
import socket
import random

from message import *

ADDRESS = sys.argv[1]
PORT = int(sys.argv[2])
FILE = sys.argv[3]

is_ipv6 = ADDRESS.count(":") > 0

is_ascii = True
for c in FILE:
  if ord(c) > 127:
    is_ascii = True
    break

if FILE.count(".") != 1 or len(FILE.split(".")[1]) < 3 or not is_ascii or len(FILE) > 15:
  print("Nome do arquivo incorreto")
  exit()

MAX_PACKET_RESENDS = 10
TIMEOUT_TIME = 1 # seconds
WAIT_TIME = 0.1 # seconds

upload_mutex = threading.Lock()

s_socket = socket.socket(socket.AF_INET6 if is_ipv6 else socket.AF_INET, socket.SOCK_STREAM)
s_socket.connect((ADDRESS, PORT))

confirmation_count = 0
sliding_window_start = 0

def connect():
  # Manda o Hello para iniciar a conexão
  s_socket.send(HelloMessage().get_message_bytes())

  # Recebe a conexão
  connection_data = s_socket.recv(ConnectionMessage.get_message_size())
  connection_type, connection_channel = MessageHelper.get_message_header(connection_data)

  # Se não recebeu a conexão, cancele
  if connection_type != MSG_TYPE_CONNECTION or connection_channel != MSG_CHANNEL_CONTROL:
    print("Erro não recebeu Connection")
    return None

  file_port = ConnectionMessage.get_message_data(connection_data)
  file_socket = socket.socket(socket.AF_INET6 if is_ipv6 else socket.AF_INET, socket.SOCK_DGRAM)

  # Enviando as informações do arquivo para o servidor
  s_socket.send(InfoFileMessage(FILE, os.path.getsize(FILE)).get_message_bytes())

  # Recebe o OK
  ok_data = s_socket.recv(OkMessage.get_message_size())
  ok_type, ok_channel = MessageHelper.get_message_header(ok_data)

  # Se não recebeu o ok, cancele
  if ok_type != MSG_TYPE_OK or ok_channel != MSG_CHANNEL_CONTROL:
    print("Erro não recebeu Ok")
    return None

  # Envia o arquivo
  send_file(file_socket, file_port)

  # Fecha a conexão
  s_socket.close()

# Envia o arquivo obtido no 'file_socket'
def send_file(file_socket, file_port):
  global sliding_window_start
  global confirmation_count

  # Abre os arquivos no modo binário
  with open(FILE, "rb") as file:

    # Inicia a thread que irá esperar pelas confirmações de recebimento
    ack_thread = threading.Thread(target=ack_handling)
    ack_thread.start()

    # Cria o objeto da mensagem e pega os bytes da mesma
    file_message_packets = FileMessage(file).get_message_bytes()

    # Enquanto o número de confirmações for menor que o número de pacotes a serem
    # enviados
    while confirmation_count < len(file_message_packets):
      upload_mutex.acquire()

      # Janela deslizante
      slide_count = min(confirmation_count + 4, len(file_message_packets))
      for i in range(sliding_window_start, slide_count):
        file_socket.sendto(file_message_packets[i], (ADDRESS, file_port))

      sliding_window_start += slide_count

      # Espera um pouco as confirmações.
      timeout_counter = time.time()
      while confirmation_count != sliding_window_start:
        if time.time() < timeout_counter + TIMEOUT_TIME:
          break

        upload_mutex.release()
        time.sleep(WAIT_TIME)
        upload_mutex.acquire()

      # Se a gente não receber alguma confirmação, volta a janela deslizante
      # para lá
      if confirmation_count != sliding_window_start:
        sliding_window_start = confirmation_count
      
      upload_mutex.release()

    ack_thread.join()
  

# Função que recebe as confirmações do recebimento dos pacotes
def ack_handling():
  global confirmation_count
  global sliding_window_start

  while True:
    # Recebe uma mensagem
    ack_data = s_socket.recv(ACKMessage.get_message_size())

    try:
      ack_type, ack_channel = MessageHelper.get_message_header(ack_data)

      # Se recebeu a mensagem de fim, saia
      if ack_type == MSG_TYPE_END and ack_channel == MSG_CHANNEL_CONTROL:
        print("Arquivo enviado com sucesso.")
        os._exit(0)
      # Se recebeu a mensagem de ACK, atualiza o ack_count e o packet_index
      elif ack_type == MSG_TYPE_ACK and ack_channel == MSG_CHANNEL_CONTROL:
        sequence_number = ACKMessage.get_message_data(ack_data)
        if sequence_number > confirmation_count:
          confirmation_count = sequence_number

        if sequence_number > sliding_window_start:
          upload_mutex.acquire()
          sliding_window_start = sequence_number
          upload_mutex.release()
    except:
      break

def main():
  connect()

main()