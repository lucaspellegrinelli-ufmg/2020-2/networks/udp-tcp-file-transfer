MSG_TYPE_HELLO = 0
MSG_TYPE_CONNECTION = 1
MSG_TYPE_INFO_FILE = 2
MSG_TYPE_OK = 3
MSG_TYPE_END = 4
MSG_TYPE_FILE = 5
MSG_TYPE_ACK = 6

MSG_CHANNEL_CONTROL = 0
MSG_CHANNEL_DATA = 1

class MessageHelper:
  @staticmethod
  def get_message_header(bytes):
    message_header = tuple(bytes[:2])
    return message_header

class Message:
  def __init__(self, type, channel):
    self.type = type
    self.channel = channel

class HelloMessage(Message):
  def __init__(self):
    super().__init__(MSG_TYPE_HELLO, MSG_CHANNEL_CONTROL)
  
  def get_message_bytes(self):
    encoded_type = self.type.to_bytes(1, byteorder="big")
    encoded_channel = self.channel.to_bytes(1, byteorder="big")
    return encoded_type + encoded_channel

  @staticmethod
  def get_message_size():
    return 2

class ConnectionMessage(Message):
  def __init__(self, udp_port):
    super().__init__(MSG_TYPE_CONNECTION, MSG_CHANNEL_CONTROL)
    self.udp_port = udp_port
  
  def get_message_bytes(self):
    encoded_type = self.type.to_bytes(1, byteorder="big")
    encoded_channel = self.channel.to_bytes(1, byteorder="big")
    encoded_udp_port = self.udp_port.to_bytes(4, byteorder="big")
    return encoded_type + encoded_channel + encoded_udp_port

  @staticmethod
  def get_message_size():
    return 6

  @staticmethod
  def get_message_data(bytes):
    udp_port_data = int.from_bytes(bytes[2:], byteorder="big")
    return udp_port_data

class InfoFileMessage(Message):
  def __init__(self, file_name, file_size):
    super().__init__(MSG_TYPE_INFO_FILE, MSG_CHANNEL_CONTROL)
    self.file_name = file_name
    self.file_size = file_size
  
  def get_message_bytes(self):
    encoded_type = self.type.to_bytes(1, byteorder="big")
    encoded_channel = self.channel.to_bytes(1, byteorder="big")
    encoded_file_name = self.file_name.encode("ascii")
    encoded_file_name = b'\x00' * (15 - len(encoded_file_name)) + encoded_file_name
    encoded_file_size = self.file_size.to_bytes(8, byteorder="big")
    return encoded_type + encoded_channel + encoded_file_name + encoded_file_size

  @staticmethod
  def get_message_size():
    return 25

  @staticmethod
  def get_message_data(bytes):
    full_data = bytes[2:]
    file_name_data = full_data[:15].decode(encoding="ascii").lstrip("\x00")
    file_size_data = int.from_bytes(full_data[15:], byteorder="big")
    return file_name_data, file_size_data

class OkMessage(Message):
  def __init__(self):
    super().__init__(MSG_TYPE_OK, MSG_CHANNEL_CONTROL)
  
  def get_message_bytes(self):
    encoded_type = self.type.to_bytes(1, byteorder="big")
    encoded_channel = self.channel.to_bytes(1, byteorder="big")
    return encoded_type + encoded_channel

  @staticmethod
  def get_message_size():
    return 2

class EndMessage(Message):
  def __init__(self):
    super().__init__(MSG_TYPE_END, MSG_CHANNEL_CONTROL)
  
  def get_message_bytes(self):
    encoded_type = self.type.to_bytes(1, byteorder="big")
    encoded_channel = self.channel.to_bytes(1, byteorder="big")
    return encoded_type + encoded_channel

  @staticmethod
  def get_message_size():
    return 2

class FileMessage(Message):
  def __init__(self, file):
    super().__init__(MSG_TYPE_FILE, MSG_CHANNEL_DATA)
    self.file = file
  
  def get_message_bytes(self):
    encoded_type = self.type.to_bytes(1, byteorder="big")
    encoded_channel = self.channel.to_bytes(1, byteorder="big")
    
    all_packets = []
    while True:
      payload_data = self.file.read(1000)

      if payload_data:
        sequence_number = len(all_packets)
        payload_size = len(payload_data)

        encoded_sequence_number = sequence_number.to_bytes(4, byteorder="big")
        encoded_payload_size = payload_size.to_bytes(2, byteorder="big")

        this_packet = encoded_type + encoded_channel + encoded_sequence_number + encoded_payload_size + payload_data
        all_packets.append(this_packet)
      else:
        break

    return all_packets

  @staticmethod
  def get_message_size():
    return None

class ACKMessage(Message):
  def __init__(self, sequence_number):
    super().__init__(MSG_TYPE_ACK, MSG_CHANNEL_CONTROL)
    self.sequence_number = sequence_number
  
  def get_message_bytes(self):
    encoded_type = self.type.to_bytes(1, byteorder="big")
    encoded_channel = self.channel.to_bytes(1, byteorder="big")
    encoded_sequence_number = self.sequence_number.to_bytes(4, byteorder="big")
    return encoded_type + encoded_channel + encoded_sequence_number

  @staticmethod
  def get_message_size():
    return 6

  @staticmethod
  def get_message_data(bytes):
    sequence_number = int.from_bytes(bytes[2:], byteorder="big")
    return sequence_number